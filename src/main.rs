use askama::Template;
use chrono::prelude::*;
use std::os::unix::fs::MetadataExt;
use std::path::PathBuf;
use std::{fs, path};

const VERSION: &str = "1.0.0";
const HELP_TEXT: &str = "\
USAGE:
  rss-folder [OPTIONS] --folder PATH_TO_FOLDER
FLAGS:
  -h, --help            Prints help information
  -v, --version         Prints the version
  -r, --readonly        Does not write the file to the folder
  -l, --list            Prints a list of all files to stdout
OPTIONS:
  --url-prefix          Url prefix before filename in the rrs-feed
  --feed-name           Name of the rss-feed
  --out-dir             Specify output directory. Default is same as --folder
  --filename            Specify filename of the generated file. Defaults to feed.rss
  --hostname            Hostname with protocoll for the rss-feed. Defaults to localhost
  --protocol            Protocoll to use. Defaults to https 
";

struct AppArgs {
    folder: PathBuf,
    readonly: bool,
    show_list: bool,
    url_prefix: String,
    hostname: String,
    feed_name: String,
    protocol: String,
    out_dir: PathBuf,
    filename: PathBuf,
}

struct File {
    name: String,
    last_updated: String,
}

#[derive(Template)]
#[template(path = "template.rss", escape = "html")]
struct RssValues<'a> {
    feed_name: &'a str,
    hostname: &'a str,
    protocol: &'a str,
    build_time: &'a str,
    url_prefix: &'a str,
    files: &'a Vec<File>,
}

fn main() {
    let args = match parse_args() {
        Ok(args) => args,
        Err(error) => {
            eprintln!("Error: {}.", error);
            eprintln!("{}", HELP_TEXT);
            std::process::exit(1);
        }
    };
    let files: Vec<File> = read_files(&args.folder);

    if args.show_list {
        for file in &files {
            println!("{}", file.name);
        }
    }

    if !args.readonly {
        let template = RssValues {
            feed_name: &args.feed_name,
            hostname: &args.hostname,
            protocol: &args.protocol,
            url_prefix: &args.url_prefix,
            build_time: &Utc::now().to_rfc2822(),
            files: &files,
        };
        let template_string = match template.render() {
            Ok(v) => v,
            Err(err) => {
                eprintln!("Error rendering Template: {}", err);
                std::process::exit(1);
            }
        };

        let out_path = path::Path::join(&args.out_dir, args.filename);
        match fs::write(&out_path, template_string) {
            Ok(v) => v,
            Err(err) => {
                eprintln!("Error writing file {}. {}", out_path.display(), err);
                std::process::exit(1);
            }
        }
    }
}

// Read all filenames form specified folder path
fn read_files(path: &PathBuf) -> Vec<File> {
    let paths = match fs::read_dir(path) {
        Ok(v) => v,
        Err(error) => {
            eprintln!("Error opening folder {}, {}", path.display(), error);
            std::process::exit(1);
        }
    };

    let mut files: Vec<File> = Vec::new();

    for path in paths {
        match path {
            Ok(path) => {
                let data = path.metadata().unwrap();
                if data.is_file() {
                    files.push(File {
                        name: path.file_name().into_string().unwrap(),
                        last_updated: DateTime::<Utc>::from_utc(
                            NaiveDateTime::from_timestamp_opt(data.ctime(), 0).unwrap(),
                            Utc,
                        )
                        .to_rfc2822(),
                    })
                }
            }
            Err(err) => eprintln!("Error {}", err),
        }
    }
    files
}

fn parse_args() -> Result<AppArgs, pico_args::Error> {
    let mut pargs = pico_args::Arguments::from_env();

    // Help has a higher priority and should be handled separately.
    if pargs.contains(["-h", "--help"]) {
        print!("{}", HELP_TEXT);
        std::process::exit(0);
    }

    if pargs.contains(["-v", "--version"]) {
        println!("rss-folder: {}", VERSION);
        std::process::exit(0);
    }
    let folder: path::PathBuf = pargs.value_from_str("--folder")?;

    let args = AppArgs {
        // Parses an optional value that implements `FromStr`.
        url_prefix: pargs
            .value_from_str("--url-prefix")
            .unwrap_or("".to_string()),
        hostname: pargs
            .value_from_str("--hostname")
            .unwrap_or("localhost".to_string()),
        protocol: pargs
            .value_from_str("--protocol")
            .unwrap_or("https".to_string()),
        feed_name: pargs
            .value_from_str("--feed-name")
            .unwrap_or("rss-folder".to_string()),
        readonly: pargs.contains(["-r", "--readonly"]),
        show_list: pargs.contains(["-l", "--list"]),
        folder: PathBuf::from(&folder), // Creating a copy, we might need it again for out_dir
        out_dir: pargs.opt_value_from_str("--out-dir")?.unwrap_or(folder),
        filename: pargs
            .opt_value_from_str("--filename")?
            .unwrap_or(PathBuf::from("feed.rss")),
    };

    let remaining = pargs.finish();
    if !remaining.is_empty() {
        eprintln!("Warning: unused arguments left: {:?}.", remaining);
    }
    Ok(args)
}
